import React from "react";
import Navigation from "../../components/Navigation";
import AppRoute from "../../routers";
import {BrowserRouter as Router} from "react-router-dom";
import {Layout} from 'antd';
import {useSelector} from "react-redux";
import AuthPage from "../Auth";

const {Content, Footer, Sider} = Layout;

export default function AppPage(props: any) {
    const user = useSelector((state: any) => state.Auth.user);

    return user ? (
        <Router>
            <Layout style={{height: '100%'}}>
                <Sider breakpoint="lg" collapsedWidth="0">
                    <div className="logo">
                        <img alt="logo"
                             src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg"/>
                        <h1>DAR CMS</h1>
                    </div>
                    <Navigation/>
                </Sider>
                <Layout>
                    <Content>
                        <AppRoute/>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>Ant Design ©2021 Created by Ant UED</Footer>
                </Layout>
            </Layout>
        </Router>
    ) : (
        <AuthPage/>
    )
}
