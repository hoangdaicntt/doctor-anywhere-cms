import React, {useEffect, useState} from "react";
import {Layout, PageHeader, Table, Tag, Space, Button, Tabs} from 'antd';
import Panel from "../../components/Containers/Panel";
import Content from "../../components/Containers/Content";
import {getUsers} from "../../services/app.service";

const {TabPane} = Tabs;
const {Header, Sider} = Layout;
export default function UserPage(props: any) {
    const [type, setType] = useState('DOCTOR')
    const [data, setData] = useState([])


    useEffect(() => {
        getUsers(type).then(value => {
            if (value) {
                setData(value.data)
            }
        })
    }, [type])

    const columns = [
        {
            title: 'ID',
            key: '_id',
            dataIndex: '_id',
        },
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username',
        },
        {
            title: 'Quickblox ID',
            dataIndex: 'qbId',
            key: 'qbId',
        },
        {
            title: 'Created At',
            dataIndex: 'createdAt',
            key: 'createdAt',
        }, {
            title: 'Type',
            dataIndex: 'type',
            key: 'type',
        },
        {
            title: 'Action',
            key: '_id',
        },
    ];


    return (
        <Layout>
            <PageHeader
                className="site-page-header"
                title="User management"
                subTitle="This is a subtitle"
                extra={[
                    <Button key="1" type="primary">Add</Button>,
                ]}
            />
            <Content>
                <Panel>
                    <Tabs defaultActiveKey="1" onChange={(e) => setType(e)}>
                        <TabPane tab="Doctor" key="DOCTOR">
                        </TabPane>
                        <TabPane tab="Agent" key="AGENT">
                        </TabPane>
                        <TabPane tab="Patient" key="PATIENT">
                        </TabPane>
                    </Tabs>
                    <Table rowKey={(row: any) => row._id} columns={columns} dataSource={data}/>
                </Panel>
            </Content>
        </Layout>
    )
}
