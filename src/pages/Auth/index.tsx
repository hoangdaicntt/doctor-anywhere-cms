import React, {useState} from "react";
import {Alert, message} from 'antd';
import {Form, Input, Button, Checkbox} from 'antd';
import './style.scss';
import {Typography} from 'antd';
import {useDispatch} from "react-redux";
import {actionAuthChange} from "../../redux/actions/auth.action";
import {login} from "../../services/app.service";

const {Title, Paragraph, Text} = Typography;
export default function AuthPage(props: any) {
    const dispatch = useDispatch();

    const layout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };
    const tailLayout = {
        wrapperCol: {offset: 8, span: 16},
    };

    const onFinish = (values: any) => {
        login(values.username, values.password).then(value => {
            if (value) {
                dispatch(actionAuthChange(value));
            } else {
                message.error(value.message || "Cannot login").then();
            }
        }).catch(reason => {
            message.error(reason.message).then();
        });
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div className="login-warp">
            <div className="login-title">
                <Title level={3}>Login Doctor Anywhere</Title>
                <Paragraph>Please login to the system to continue</Paragraph>
            </div>
            <Form
                {...layout}
                name="basic"
                initialValues={{remember: true}}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="Username"
                    name="username"
                    rules={[{required: true, message: 'Please input your username!'}]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{required: true, message: 'Please input your password!'}]}
                >
                    <Input.Password/>
                </Form.Item>

                <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                    <Checkbox>Remember me</Checkbox>
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}
