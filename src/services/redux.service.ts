let globalStore: any = null;

export function setStore(s: any) {
    globalStore = s;
}

export function getState() {
    return globalStore.getState();
}
