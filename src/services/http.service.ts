import axios from 'axios';
import {getState} from "./redux.service";

let TOKEN = "";

const headers = () => {
    return {
        'authorization': `${getState()?.Auth?.user?.token || ""}`,
    }
};

export default class HttpService {

    static get(url: string) {
        return axios.get(url, {
            headers: headers(),
        }).then(value => {
            return value.data;
        }).catch(error => {
            return error;
        });
    }

    static post(url: string, data: any) {
        return axios.post(url, data, {
            headers: headers(),
        }).then(value => {
            return value.data;
        }).catch(error => {
            return error;
        });
    }

    static put(url: string, data: any) {
        return axios.put(url, data, {
            headers: headers(),
        }).then(value => {
            return value.data;
        }).catch(error => {
            return error;
        });
    }

    static delete(url: string) {
        return axios.delete(url, {
            headers: headers(),
        }).then(value => {
            return value.data;
        }).catch(error => {
            return error;
        });
    }
}
