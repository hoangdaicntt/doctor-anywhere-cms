import HttpService from "./http.service";
import Environment from "../environment";

export function login(username: string, password: string) {
    return HttpService.post(Environment.host.concat(`/admin/auth/login`), {username, password})
}

export function getUsers(type: string = 'DOCTOR') {
    return HttpService.get(Environment.host.concat(`/admin/users?type=${type}`))
}
