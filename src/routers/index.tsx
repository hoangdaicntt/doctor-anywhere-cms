import {Switch, Route,} from "react-router-dom";
import HomePage from "../pages/Home";
import UserPage from "../pages/User";
import ConsultPage from "../pages/Consult";

export default function AppRoute(props: any) {
    return <Switch>
        <Route path="/home">
            <HomePage/>
        </Route>
        <Route path="/users">
            <UserPage/>
        </Route>
        <Route path="/consults">
            <ConsultPage/>
        </Route>
    </Switch>
}
