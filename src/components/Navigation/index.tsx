import React, {useState} from "react";
import {Menu} from "antd";
import {
    CarryOutOutlined,
    UserOutlined,
    DatabaseOutlined,
    PicRightOutlined,
    FlagOutlined,
    SendOutlined,
    UsergroupAddOutlined,
    ContactsOutlined,
    AppstoreOutlined
} from "@ant-design/icons";
import {Link} from "react-router-dom";

const {SubMenu} = Menu;

export default function Navigation(props: any) {
    return (<Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
        <Menu.Item key="1" icon={<AppstoreOutlined/>}>
            <Link to='/'>Dashboard</Link>
        </Menu.Item>
        <Menu.Item key="2" icon={<UserOutlined/>}>
            <Link to='/users'>Users</Link>
        </Menu.Item>
        <Menu.Item key="3" icon={<ContactsOutlined />}>
            <Link to='/consults'>Consults</Link>
        </Menu.Item>
    </Menu>)
}
