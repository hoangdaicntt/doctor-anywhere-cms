import React from 'react';
import './App.css';
import {Provider} from "react-redux";
import storeRedux from "./redux/store.redux";
import 'antd/dist/antd.css';
import 'ant-design-pro/dist/ant-design-pro.css';
import AppPage from "./pages/App";
import {setStore} from "./services/redux.service";

setStore(storeRedux);

function App() {

    return (
        <div className="app">
            <Provider store={storeRedux}>
                <AppPage/>
            </Provider>
        </div>

    );
}

export default App;
